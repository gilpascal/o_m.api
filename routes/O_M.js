require('dotenv').config();
var request = require('request')
var express = require('express')
var router = express.Router()
var HttpStatus = require('../HttpStatus/http_status')


router.get("/token", (req,res)=>{
    //var Authorization = 'Basic ' + process.env.Authorization_header;
    //const uRl= ;
    //var grant =req.body.grant_type;
    var a = req.headers
    var b = req.params.grant_type
    const options = {
        url: process.env.URL_TOKEN ,
        headers: {
            'authorization':a.authorization,
            'Content-Type':'application/x-www-form-urlencoded'
                 
        },

        form_params: {
            'grant_type': 'client_credentials' ,//b,/*process.env.grant_type,*/
                
        },



        method:'POST'

        
    }
    //console.log(options)
    
    request(options,(err,response,body)=>{
        //var want = JSON.parse(tre)
        console.log(options)
        console.log(response)
        console.log(body)
        //res.send(result)
      
        if(!err && response.statusCode == 200){
            res.status(HttpStatus.OK).json({
                success: true,
                data: body
            })
            
        }else{
            res.status(HttpStatus.badRequest).json({
                success:false,
                error:err
            })
        }
        
    })


});

router.post("/payment",(req,res)=>{
    
    const bod = {
        'merchant_key': process.env.Merchant_Key,
        'currency'   : 'OUV' ,
        'order_id'   : req.body.order_id ,
        'amount'     : 1, 
        'return_url' : 'https://www.aladin.ci/return', 
        'cancel_url' : 'https://www.aladin.ci/cancel',  
        'notif_url'  : 'https://www.aladin.ci/notif',    
        'lang'       : 'fr',

    }
    
    const options = {
        url: process.env.URL_PAY ,
        headers: {
            'Authorization': req.headers.Authorization,
                
        },

        form_params: {
            'grant_type':'client_credentials',
                
        },
        method:"POST"
    }
        
    
})


module.exports = router;